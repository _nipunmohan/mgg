package mtg.tw.nipunmohan.mtg;

/**
 * Created by NipunMohan on 03/02/2017.
 */


/**
 * This class contains all the constants used in the application
 */
public class Constants {
    public static final String HOW_MUCH_IS = "how much is";
    public static final String HOW_MANY_CREDITS_IS = "how many credits is";
    public static final String CREDITS = " credits";
    public static final String _IS = " is ";
    public static final String NO_IDEA = "I have no idea what you are talking about";
}

package mtg.tw.nipunmohan.mtg;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;
import java.net.URI;

/**
 * This is the main class of the application
 * Inflated a view which used to display output
 */
public class MainActivity extends AppCompatActivity {

    private Button btnShowOutput;
    private Button btnSelectFile;
    private TextView txtOutput;
    private WebView webview;


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnShowOutput = (Button) findViewById(R.id.btn_show_output);
        btnSelectFile = (Button) findViewById(R.id.btn_select_file);
        txtOutput = (TextView) findViewById(R.id.txt_output);
        webview = (WebView) findViewById(R.id.web_output);


        btnShowOutput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                txtOutput.setText(ShowOutput.processOutput());
                webview.loadData(ShowOutput.processOutput(), "text/html", "UTF-8");
            }
        });
        btnSelectFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performFileSearch();
            }
        });
    }

    private static final int READ_REQUEST_CODE = 42;

    /**
     * Fires an intent to spin up the "file chooser" UI and select a file.
     */
    public void performFileSearch() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent resultData) {
        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri;
            if (resultData != null) {
                uri = resultData.getData();
                Log.i("", "Uri: " + uri.getPath());
                // File processing starts here
                ProcessInputData processInputData = new ProcessInputData();
                try {

//                    File newFile = new File(Environment.getExternalStorageDirectory(),uri.getPath());
//                    Log.e("",newFile.getAbsolutePath());
                    processInputData.readFileAndProcess(MainActivity.this, uri);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }


}

package mtg.tw.nipunmohan.mtg;

/**
 * Created by NipunMohan on 03/02/2017.
 */

import android.app.Activity;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class to read and process the data at the same time
 * The processing starts as soon as the input is received
 */
public class ProcessInputData {


    private List<String> outputValues;
    private RomanAndNumaricCalculator romanAndNumaricCalculator;
    public static boolean flag = true;
    /**
     * reads and process the input file
     *
     * @param uri
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void readFileAndProcess(Activity activity, Uri uri) throws Exception {

        try {
            InputStream inputStream = activity.getContentResolver().openInputStream(uri);
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    inputStream));
            String line;
            while ((line = br.readLine()) != null) {
                if (line.contains(",")) {
                    String[] inputs = Utils.alignInputs(line.trim());
                    for (String ln : inputs)
                        processInput(ln.trim());
                } else {
                    processInput(line.trim());
                }
            }


        } catch (IOException e) {
            System.out.println("File not found exception " + e);
        }
    }

    /**
     * Parses the input line by line and decides the type of request and appropriately forwards the request
     *
     * @param line
     * @throws Exception
     */
    private void processInput(String line) throws Exception {
        romanAndNumaricCalculator = new RomanAndNumaricCalculator();
        List<String> inputQuestions = ShowOutput.inputQuestions;
        outputValues = ShowOutput.outputValues;
        line = line.toLowerCase();
        line = Utils.removeExtraSpaces(line);
        line = line.trim();

        if (line.startsWith(Constants.HOW_MANY_CREDITS_IS)) {
            inputQuestions.add(line);
            processQueries(line);
        } else if (line.startsWith(Constants.HOW_MUCH_IS)) {
            inputQuestions.add(line);
            processQueries(line);
        } else if (line.contains(Constants._IS) && !line.contains(Constants.CREDITS)) {
            Utils.getGalaxyCodes(line);
        } else if (line.contains(Constants._IS) && line.contains(Constants.CREDITS)) {
            Utils.getObjectPrice(line);
        } else {
            inputQuestions.add(line);
            outputValues.add(Constants.NO_IDEA);
        }

    }

    /**
     * Processing quries
     *
     * @param line
     */
    private void processQueries(String line) {
        flag = false;
        String[] objects = line.split(" ");
        StringBuilder romanVal = new StringBuilder();
        String finalValue = "";
        for (String obj : objects) {
            if (Utils.galaxyCodes.containsKey(obj)) {
                romanVal.append(Utils.galaxyCodes.get(obj));
            } else if (obj.equalsIgnoreCase("?")) {
                if (romanAndNumaricCalculator.validateRomanValue(romanVal.toString())) {
                    int decVal = romanAndNumaricCalculator.romanToDecimal(romanVal.toString());
                    finalValue = String.valueOf(decVal);
                    outputValues.add(finalValue);
                } else {
                    outputValues.add(Constants.NO_IDEA);
                }
            } else if (Utils.objectPrice.containsKey(obj)) {
                if (Utils.objectPrice.get(obj) != -1.0) {
                    if (romanAndNumaricCalculator.validateRomanValue(romanVal.toString())) {
                        double currObjectValue = Utils.objectPrice.get(obj);
                        int toatalObjectsToSell = romanAndNumaricCalculator.romanToDecimal(romanVal.toString());
                        finalValue = String.valueOf(currObjectValue * toatalObjectsToSell);
                        outputValues.add(finalValue);
                    } else {
                        outputValues.add(Constants.NO_IDEA);
                    }
                } else {
                    outputValues.add(Constants.NO_IDEA);
                }
                break;
            }
        }
    }
}

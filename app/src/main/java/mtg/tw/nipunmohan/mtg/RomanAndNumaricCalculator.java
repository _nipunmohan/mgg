package mtg.tw.nipunmohan.mtg;

import java.util.regex.Pattern;

/**
 * Created by NipunMohan on 03/02/2017.
 */

/**
 * This class contains all the methods to manipulates and validate roman values
 */
public class RomanAndNumaricCalculator {

   /**
     * Convert Roman Values to Decimal Values
      * @param romanValue
     * @return
     */
    public int romanToDecimal(String romanValue) {
        int decimalNum = 0;
        int l = romanValue.length();
        int num = 0;
        int previousNum = 0;
        for (int i = l - 1; i >= 0; i--) {
            char x = romanValue.charAt(i);
            x = Character.toUpperCase(x);
            switch (x) {
                case 'I':
                    previousNum = num;
                    num = 1;
                    break;
                case 'V':
                    previousNum = num;
                    num = 5;
                    break;
                case 'X':
                    previousNum = num;
                    num = 10;
                    break;
                case 'L':
                    previousNum = num;
                    num = 50;
                    break;
                case 'C':
                    previousNum = num;
                    num = 100;
                    break;
                case 'D':
                    previousNum = num;
                    num = 500;
                    break;
                case 'M':
                    previousNum = num;
                    num = 1000;
                    break;
            }
            if (num < previousNum) {
                decimalNum = decimalNum - num;
            } else
                decimalNum = decimalNum + num;
        }
        return decimalNum;
    }

    /**
     * Method used to check whether the roman value generates is valid or not
     * @param romanVal
     * @return
     */
    public boolean validateRomanValue(String romanVal){
        Pattern regex = Pattern.compile("M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})");
        return romanVal!=null && !"".equals(romanVal) && regex.matcher(romanVal.toUpperCase()).matches();
    }
}

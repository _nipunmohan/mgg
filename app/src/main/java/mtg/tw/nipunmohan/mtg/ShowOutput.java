package mtg.tw.nipunmohan.mtg;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NipunMohan on 03/02/2017.
 */

/**
 * This class is for show the final output
 */
public class ShowOutput {



    public static final List<String> inputQuestions = new ArrayList<>();
    public static final List<String> outputValues = new ArrayList<>();

    /**
     * Display the output
     */
    public static String processOutput(){
        StringBuilder result = new StringBuilder();
        if(ProcessInputData.flag){
            result.append("<b> Invalid Input </b><br><br>");
            return result.toString();
        }
        result.append("<b> Test Input </b><br><br>");
        for(String ip : inputQuestions){
            result.append(ip).append("<br>");
        }
        result.append("<br><br>").append("<b> Test Output </b><br><br>");


        for (int i = 0; i < inputQuestions.size(); i++) {
            String question = inputQuestions.get(i).replace("?","");
            String output = outputValues.get(i);
            if(output == Constants.NO_IDEA ){
                result.append(Constants.NO_IDEA).append("<br>");
            }
            else{
                if(question.startsWith(Constants.HOW_MUCH_IS)){
                    result.append(question.substring(Constants.HOW_MUCH_IS.length(), question.length()-1).trim());
                    result.append(Constants._IS);
                    result.append(Double.valueOf(output).intValue()).append("<br>");
                }

                else if(question.startsWith(Constants.HOW_MANY_CREDITS_IS)){
                    result.append(question.substring(Constants.HOW_MANY_CREDITS_IS.length(), question.length()-1).trim());
                    result.append(Constants._IS);
                    result.append(Double.valueOf(output).intValue());
                    result.append(Constants.CREDITS).append("<br>");
                }
                else
                    result.append(Constants.NO_IDEA).append("<br>");
            }
            System.out.println(result.toString());

        }
        return  result.toString();
    }

}
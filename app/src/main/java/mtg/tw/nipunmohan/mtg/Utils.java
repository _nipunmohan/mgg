package mtg.tw.nipunmohan.mtg;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by NipunMohan on 04/02/2017.
 */

/**
 * This class contains all generic methods used in the application.
 */
public class Utils {

    public static Map<String, Character> galaxyCodes = new HashMap<>();
    public static Map<String, Double> objectPrice = new HashMap<>();
    public static RomanAndNumaricCalculator romanAndNumaricCalculator = new RomanAndNumaricCalculator();
    public static List<String> romanLetters = Arrays.asList("I", "V", "X", "L", "C", "D", "M");

    /**
     * Remove the extra spaces in between the words in a statement
     * @param str
     * @return
     */
    public static String removeExtraSpaces(String str) {
        StringBuilder sb = new StringBuilder();
        for (String s : str.split(" ")) {

            if (!s.equals(""))
                sb.append(s + " ");

        }
        return new String(sb.toString());
    }

    /**
     * Extract credit value from the statements given
     * @param line
     * @return
     */
    public static double getCreditValue(String line) {
        int creditValue = 0;

        Pattern p = Pattern.compile("(\\d+)");
        Matcher m = p.matcher(line);
        while (m.find()) {
            System.out.println(m.group(1));
            creditValue = Integer.parseInt(m.group(1));
        }
        return creditValue;
    }

    /**
     * Extract all galaxy - roman letter mappings
     * @param line
     */
    public static void getGalaxyCodes(String line) {
        galaxyCodes.put(line.split(" ")[0], line.split(" ")[2].toUpperCase().charAt(0));
    }

    /**
     * Extract particular object price to be sold
     * @param line
     */
    public static void getObjectPrice(String line) {

        String[] objects = line.split(" ");
        StringBuilder romanValue = new StringBuilder();
        for (String obj : objects) {
            if (galaxyCodes.containsKey(obj)) {
                romanValue.append(galaxyCodes.get(obj));
            } else {
                if (romanAndNumaricCalculator.validateRomanValue(romanValue.toString())) {
                    int objectCount = romanAndNumaricCalculator.romanToDecimal(romanValue.toString());
                    Double totalObjectPrice = getCreditValue(line);
                    objectPrice.put(obj, (totalObjectPrice / objectCount));
                } else {
                    objectPrice.put(obj, -1.0);
                }
                break;
            }
        }
    }

    /**
     * Reformat the inputs if it is comma seperated
     * @param line
     * @return
     */
    public static String[] alignInputs(String line) {
        String[] inputs;
        if (line.contains(",")) {
            inputs = line.split(",");
            return inputs;
        } else {

        }
        return new String[0];
    }


}
